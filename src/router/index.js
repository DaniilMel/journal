import Vue from "vue";
import Router from "vue-router";

import HomeView from "@/views/HomeView.vue";
import TablesView from "@/views/TablesView.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/tables',
      name: 'tables',
      component: TablesView
    },
  ]
});
