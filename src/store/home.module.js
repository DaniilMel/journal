import { generateData, tables as exampData } from "@/common/examples";
import {
   /*    mainTableResponse,
    getNameByField,
    getTypeByField,*/
  getMainTableTemplate, 
  //getTableNameByField
} from "@/common/response.temp";
import { MainTableService, getDeviceName } from "@/common/api.service";

const state = {
  mainTable: generateData(100),

  filter: getMainTableTemplate(),
  selectedData: undefined,
  showEditForm: false,
  showFilter: false,

  responseStatuses: [],
};

const getters = {
  mainTable(state) {
    return state.mainTable;//
    //return generateData(1000);
  },
  selectedData(state) {
    return state.selectedData;
  },
  filter(state) {
    
    return state.filter;
  },
  selectedFilter(state) {
    return state.filter;
  },
  showEditForm(state) {
    return state.showEditForm;
  },
  showFilter(state) {
    return state.showFilter;
  },
};

const actions = {
  getMainTableData(context, params) {
    MainTableService.getNotes(params.page, params.sort, params.mask)
      .then((data) => {
        context.commit('setMainTable', data);
      })
      .catch((e) => { });
  },
  addNoteToMainTable(context, noteData) {
    MainTableService.addNote(noteData)
      .then((response) => {
      })
      .catch((e) => { });
  },
  updateNoteInMainTable(context, noteData) {
    MainTableService.postNote(noteData)
      .then((response) => {
      })
      .catch((e) => { });
  },
  deleteNoteInMainTable(context, idObj) {
    MainTableService.delNote(idObj)
      .then((response) => {
      })
      .catch((e) => { });
  },

  openEditForm(context, data) {
    if (data) {
      context.commit('selectData', data);
    }
    context.state.showEditForm = true;
  },
  closeEditForm(context) {
    context.state.selectedData = undefined;
    context.state.showEditForm = false;
  },
  openFilter(context) {
    context.state.showFilter = true;
  },
  closeFilter(context) {
    context.state.showFilter = false;
  },
};

const mutations = {
  setFilter(state, filter) {
    state.filter = Object.assign({}, filter);
  },
  selectData(state, data) {
    state.selectedData = Object.assign({}, data);
  },
  setMainTable(state, data) {
    state.mainTable = data;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
