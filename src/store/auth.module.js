import ApiService, { getDeviceName } from "@/common/api.service";
import JwtService from "@/common/jwt.service";

const state = {
  user: {},
  errors: {},

  isAuth: !!JwtService.getToken(),
  showLoginForm: false,

  isAuthenticated: !!JwtService.getToken()
};

const getters = {
  showLoginForm(state) {
    return state.showLoginForm;
  },
  isAuth(state) {
    return state.isAuth;
  },
};

const actions = {
  openLoginForm(context) {
    context.state.showLoginForm = true;
  },
  closeLoginForm(context) {
    context.state.showLoginForm = false;
  },
  login(context, logData = {}) {
    //context.commit('setAuth', { name: 'Ф.И.О' });

    ApiService.post(`${getDeviceName('DOMEN')}/DOMEN`, JSON.stringify(logData))
      .then(({ data }) => {
        
        let user = JSON.parse(data.response);
        console.log(user);
        context.commit('setAuth', user);
        resolve(data);
      })
      .catch(({ response }) => {
        //context.commit(SET_ERROR, response.data.ErrorToUser);
      });

    context.dispatch('closeLoginForm');
  },
  logOut(context) {
    context.commit('logOut');
  },
  checkAuth(context) {
    console.log(JwtService.getToken());
    
    if (JwtService.getToken()) {
      ApiService.setHeader();

      return ApiService.get(`${getDeviceName('DOMEN')}/DOMEN`)
        .then(({ data }) => {
          let user = JSON.parse(data.response);
        
          context.commit('setAuth', user);
        })
        .catch(({ response }) => {
          //context.commit(SET_ERROR, response);
        });
    } else {
      context.commit('logOut');
    }
  },
};

const mutations = {
  setAuth(state, userData) {
    state.user = userData;
    state.isAuth = true;
    JwtService.saveToken(state.user.jwt);
  },
  logOut(state) {
    state.isAuth = false;
    state.user = {};
    JwtService.destroyToken();
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};