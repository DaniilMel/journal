import {TablesService, getDeviceName} from "@/common/api.service";
import { tables as exampData } from "@/common/examples";

const state = {
  tables: exampData,
};

const getters = {
  tables(state) {
    return state.tables;
  },
};

const actions = {
  getTables(context) {
    TablesService.getTables()
      .then((data) => {
        context.commit('setTables', data);
      })
      .catch((e) => { });
  },
  addNoteToSupportTable(context, noteData) {
    TablesService.addField(noteData)
      .then((response) => {
      })
      .catch((e) => { });
  },
  updateNoteInSupportTable(context, noteData) {
    TablesService.postField(noteData)
      .then((response) => {
      })
      .catch((e) => { });
  },
  deleteNoteInSupportTable(context, params) {
    TablesService.delField(params)
      .then((response) => {
      })
      .catch((e) => { });
  },
};

const mutations = {
  setTables(state, tables) {
    state.tables = tables;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
