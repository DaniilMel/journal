import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import ElementUI from 'element-ui';
import lang from 'element-ui/lib/locale/lang/ru-RU'
import locale from 'element-ui/lib/locale'
import 'element-ui/lib/theme-chalk/index.css';
locale.use(lang);
Vue.config.productionTip = false;
Vue.use(ElementUI);

import apiService from './common/api.service'
apiService.init();

router.beforeEach((to, from, next) =>
  Promise.all([store.dispatch('checkAuth')]).then(next)
);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
