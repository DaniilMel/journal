import {
    mainTableResponse,
    getNameByField,
    getTypeByField,
    getMainTableTemplate,
    getTableNameByField,
    getFilterStyleByField,
    getTableDataByName,
} from "@/common/response.temp";

export const data = [
    {
        id: 0,
        author: "anon0",
        authorId: "0",
        theme: "#example",
        text:
            "Наряду с типами",
        sector: "Moskow",
        number: 10,
        time: "1h",
        num: 0,
        status: 0,
    },
    {
        id: 1,
        author: "anon1",
        authorId: "1",
        theme: "#example,#nextExample",
        text:
            "Наряду с типами",
        sector: "Moskow",
        number: 20,
        time: "4h",
        num: 0,
        status: 0,
    },
    {
        id: 2,
        author: "anon2",
        authorId: "0",
        theme: "#example,#other",
        text:
            "Наряду с типами",
        sector: "Moskow",
        number: 30,
        time: "6h",
        num: 0,
        status: 0,
    },
    {
        id: 3,
        author: "anon3",
        authorId: "1",
        theme: "#example",
        text: "Наряду с типами",
        sector: "Moskow",
        number: 40,
        time: "8h",
        num: 0,
        status: 0,
    },
    {
        id: 4,
        author: "anon4",
        authorId: "1",
        theme: "#example",
        text: "Наряду с типами",
        sector: "Moskow",
        number: 40,
        time: "8h",
        num: 0,
        status: 0,
    },
    {
        id: 5,
        author: "anon5",
        authorId: '0',
        theme: "#example",
        text: "Наряду с типами",
        sector: "Moskow",
        number: 40,
        time: "8h",
        num: 0,
        status: 0,
    },
    {
        id: 6,
        author: "anon6",
        authorId: '1',
        theme: "#example",
        text: "Наряду с типами",
        sector: "Moskow",
        number: 40,
        time: "8h",
        num: 0,
        status: 0,
    },
    {
        id: 7,
        author: "anon7",
        authorId: "1",
        theme: "#example",
        text: "Наряду с типами",
        sector: "Moskow",
        number: 40,
        time: "8h",
        num: 0,
        status: 0,
    }
];

export const tables = [
    {
        id: 0,
        name: "types",
        data: [{ id: 0, value: 'Текстовый' }, { id: 1, value: "КД" }, { id: 2, value: "ПД" }],
    },
    {
        id: 1,
        name: "indexes",
        data: [{ id: 0, value: 'ЛГ' }, { id: 1, value: 'ПГ' }, { id: 2, value: 'АБВГД' }, { id: 3, value: 'ОРП' }, { id: 4, value: "ЩНШ" }],
    },
    {
        id: 2,
        name: "sectors",
        data: [{ id: 0, value: '170' }, { id: 1, value: '130' }, { id: 2, value: '110' }, { id: 3, value: '120' }, { id: 4, value: '150' }],
    },
    {
        id: 3,
        name: "authors",
        data: [{ id: 0, value: 'автор1' }, { id: 1, value: 'автор2' }, { id: 2, value: 'автор3' }, { id: 3, value: 'автор4' }, { id: 4, value: 'автор5' }, { id: 5, value: 'автор6' }, { id: 6, value: 'автор7' }, { id: 7, value: 'автор8' }, { id: 8, value: 'автор9' }],
    },
    {
        id: 4,
        name: "statuses",
        data: [{ id: 0, value: 'в разработке' }, { id: 1, value: "выпущена" }, { id: 2, value: "аннулирована" }],
    },
];

var arr = [];
export function generateData(count) {
    arr = [];
    for (let i = 0; i < count; i++) {
        let obj = getMainTableTemplate();

        
        for (let f in mainTableResponse) {
            let field = mainTableResponse[f];

            if (getTypeByField(field) == 'table') {
                let name = getTableNameByField(field);
                let table = getTableDataByName(tables, name);
                let index = Math.round(Math.random() * (table.length - 1))
                let val = table[index].value;
                obj[field] = val;
            } else if (getTypeByField(field) == 'input') {
                if (field == [mainTableResponse.NAME]) {
                    obj[field] = 'длинное название длинное название длинное название длинное название длинное название '
                } else if (field == [mainTableResponse.NUMBER]) {
                    obj[field] = "111-10"
                }

                else {
                    obj[field] = Math.round(Math.random() * 30);
                }

            } else {
                let curr_date = Math.round(Math.random() * 30 + 1);
                curr_date = curr_date < 10 ? ('0' + curr_date) : curr_date;
                let curr_month = Math.round(Math.random() * 11 + 1);
                curr_month = curr_month < 10 ? ('0' + curr_month) : curr_month;
                let curr_year = Math.round(Math.random() * 4) + 2015;

                obj[field] = `${curr_date}-${curr_month}-${curr_year}`;
            }
        }
        obj.id = Math.round(Math.random() * 2000);
        
        arr.push(obj);//getMainTableTemplate('placeholder')
    }
    return arr;
}