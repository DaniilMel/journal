import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import JwtService from "@/common/jwt.service";
import { API_URL } from "./config.js";

const ApiService = {
    init() {
        Vue.use(VueAxios, axios);
        Vue.axios.defaults.baseURL = API_URL;
    },

    setHeader() {
        Vue.axios.defaults.headers.common[
            "Authorization"
        ] = `Bearer ${JwtService.getToken()}`;
    },

    /*     query(resource, params) {
            return Vue.axios.get(resource, params).catch(error => {
                throw new Error(`[RWV] ApiService ${error}`);
            });
        }, */

    get(resource, slug = "") {
        //console.log(resource, slug);
        return Vue.axios.get(`${resource}?params=${slug}`).catch(error => {
            throw new Error(`[RWV] ApiService ${error}`);
        });
    },

    post(resource, params = "{}") {
        return Vue.axios.post(`${resource}?params=${params}`);
    },

    /*     update(resource, slug, params) {
            return Vue.axios.put(`${resource}/${slug}`, params);
        },
    
        put(resource, params) {
            return Vue.axios.put(`${resource}`, params,
            {headers: { 'content-type': 'multipart/form-data' }});
        }, */

    delete(resource, slug = "{}") {
        return Vue.axios.delete(`${resource}?params=${slug}`).catch(error => {
            throw new Error(`[RWV] ApiService ${error}`);
        });
    }
};

export default ApiService;

export const MainTableService = {
    getNotes(pageObj, sortObj, mask) {
        return ApiService.get('GETNOTES', JSON.stringify({
            "page": pageObj,
            "sort": sortObj,
            "mask": mask
        }));
    },
    addNote(dataObj) {
        return ApiService.post('ADDNOTE', JSON.stringify(dataObj));
    },
    postNote(dataObj) {
        return ApiService.post('POSTNOTE', JSON.stringify(dataObj));
    },
    delNote(idObj) {
        return ApiService.delete('DELNOTE', JSON.stringify(idObj));
    },
}

export const TablesService = {
    getTables() {
        return ApiService.get('GETTABLES');
    },
    addField(dataObj) {
        return ApiService.post('ADDFIELD', JSON.stringify(dataObj));
    },
    postField(dataObj) {
        return ApiService.post('POSTFIELD', JSON.stringify(dataObj));
    },
    delField(idObj) {
        return ApiService.delete('DELFIELD', JSON.stringify(idObj));
    },
}

var  devNames = {
    DOMEN: 'npo',
    GETNOTES: 'npo',
    ADDNOTE: 'npo',
    POSTNOTE: 'npo',
    DELNOTE: 'npo',
    GETTABLES: 'npo',
    ADDFIELD: 'npo',
    POSTFIELD: 'npo',
    DELFIELD: 'npo',
}
export function getDeviceName(command) {
    return devNames[command];
}