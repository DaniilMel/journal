export const mainTableResponse = {
    DATE: 'time',
    TYPE: 'type',
    THEME: 'theme',
    NUMBER: 'number',
    NAME: 'text',
    SECTOR: 'sector',
    AUTHOR: 'author',
    STATUS: 'status',
    NUMBERPUBL: 'num',  
}

const mainTableHideedFields = {
    [mainTableResponse.ID]: 'id',
}

const mainTableFields = {
    [mainTableResponse.DATE]: 'Дата',
    [mainTableResponse.TYPE]: 'Вид документа',
    [mainTableResponse.THEME]: 'Индекс темы',
    [mainTableResponse.NUMBER]: 'Номер документа',
    [mainTableResponse.NAME]: 'Наименование документа',
    [mainTableResponse.SECTOR]: 'Сектор',
    [mainTableResponse.AUTHOR]: 'Ф.И.О',
    [mainTableResponse.STATUS]: 'Статус',
    [mainTableResponse.NUMBERPUBL]: '№ документа выпуска',  
}

const mainTableInputType = {
    [mainTableResponse.DATE]: 'date',
    [mainTableResponse.TYPE]: 'table',
    [mainTableResponse.THEME]: 'table',
    [mainTableResponse.NUMBER]: 'input', 
    [mainTableResponse.NAME]: 'input', 
    [mainTableResponse.SECTOR]: 'table',
    [mainTableResponse.AUTHOR]: 'table',
    [mainTableResponse.STATUS]: 'table',
    [mainTableResponse.NUMBERPUBL]: 'input', 
}

const mainTableFieldsTypes = {
    [mainTableResponse.DATE]: 'date',
    [mainTableResponse.TYPE]: 'string',
    [mainTableResponse.THEME]: 'string',
    [mainTableResponse.NUMBER]: 'number', 
    [mainTableResponse.NAME]: 'string', 
    [mainTableResponse.SECTOR]: 'number',
    [mainTableResponse.AUTHOR]: 'string',
    [mainTableResponse.STATUS]: 'string',
    [mainTableResponse.NUMBERPUBL]: 'number', 
}

const tablesNames = { 
    [mainTableResponse.TYPE]: 'types',
    [mainTableResponse.THEME]: 'indexes',
    [mainTableResponse.SECTOR]: 'sectors',
    [mainTableResponse.AUTHOR]: 'authors',
    [mainTableResponse.STATUS]: 'statuses',
};

const mainTableStyles = {
    [mainTableResponse.DATE]: "width: 90px;",
    [mainTableResponse.TYPE]: "width: 90px;",
    [mainTableResponse.THEME]: "width: 100px;",
    [mainTableResponse.NUMBER]: "width: 160px;",
    [mainTableResponse.NAME]: "width: 450px;",
    [mainTableResponse.SECTOR]: "width: 90px;",
    [mainTableResponse.AUTHOR]: "width: 250px;",
    [mainTableResponse.STATUS]: "width: 120px;",
    [mainTableResponse.NUMBERPUBL]: "width: 200px;",
};

const mainFilterStyles = {
    [mainTableResponse.DATE]: "width: 140px !important;",
    [mainTableResponse.TYPE]: "width: 100px !important;",
    [mainTableResponse.THEME]: "width: 80px !important;",
    [mainTableResponse.NUMBER]: "width: 50px !important;",
    [mainTableResponse.NAME]: "width: 450px !important;",
    [mainTableResponse.SECTOR]: "width: 90px !important;",
    [mainTableResponse.AUTHOR]: "width: 230px !important;",
    [mainTableResponse.STATUS]: "width: 130px !important;",
    [mainTableResponse.NUMBERPUBL]: "width: 180px !important;",
};
//_________________________all____________________________________

//_________________________maintable_________________________________
export function getNameByField(field) {
    return mainTableFields[field];
}
export function getTableStyleByField(field) {
    return mainTableStyles[field];
}
export function getTypeByField(field) {
    return mainTableInputType[field];
}
export function getDataTypeByField(field) {
    return mainTableFieldsTypes[field];
}
export function getMainTableTemplate(placeholder = '') {
    let obj = {};
    for(let field in mainTableResponse) {
        if(mainTableResponse.ID == field) continue;

        obj[mainTableResponse[field]] = placeholder;     
    }
    return obj;
}
//_________________________filter_____________________________________
export function getFilterStyleByField(field) {
    return mainFilterStyles[field];
}
//______________________support tables_________________________________
export function getTableNameByField(field) {
    return tablesNames[field];
}
export function getTablesNames() {
    let arr = [];
    for(let i in mainTableResponse) {
        let field = mainTableResponse[i];
        if(getTypeByField(field) == 'table') {
            arr.push({
                name: getTableNameByField(field),
                value: getNameByField(field)
            })
        }
    }
    return arr;
}
export function getTableDataByName(tables, name) {
    let table = tables.find((t)=>{ return t.name == name; });

    if(table) return table.data;

    return [];
}
export function getTableByName(tables, name) {
    return tables.find((t)=>{ return t.name == name; });
}